# Caughley Shopify Template
The template is built using a npm, gulp and themekit workflow.

## Setup
Please install npm, gulp and themekit.
- [Homebrew](https://brew.sh/)
- [NPM](https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/)
- [Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
- [Themekit](https://shopify.github.io/themekit/)

## Watching files
Before proceeding commands below, please ensure that the prerequisites are all installed.

`mkdir ~/caughley-dev`

`git clone git@gitlab.com:igi.manaloto/caughley-shopify.git ~/cauhgley-dev`

`cd ~/caughley-dev`

`npm install`

`gulp` - this runs 'gulp watch' which builds your files while you're working.

Open a new console afterwards

`cd ~/cauhgley-dev/dist`

`theme watch` - this uploads files on the dist directory as they get changed.

## Release
On the console, cd into the base dir `cd ~/caughley-dev` and execute `gulp production`

### Caution
Changing the settings schema may cause the site to reset all settings. Be careful!

## Configuration
`dist/config.yml` - Controls shopify API configurations

`gulpfile.js` - The gulp file that houses a lot of the builder logic/workflows

`config.json` - Pulled in by gulpfile for configuration stuff. E.g. project paths etc.

## Workflow notes
`~/dist` - this is an exact copy of what you have on the live shopify template.

`~/src` - src files are here. Please inspect.

You may edit files on the dist, except the files in the assets folder ~ this is what gulp builds.

In the src/pug folder, there are pug files that get built into shopify liquid files. Please inspect them to know which files will be replaced when running gulp. (e.g. src/pug/snippets/footer.pug will be built to dist/snippets/footer.liquid)

** Don't forget to update config.yml with the api keys for your own published theme.

## Tree dump:
<pre>
.
├── dist - this is where all the shopify files are
│   ├── assets - IMPORTANT - DO NOT EDIT ANY FILES HERE. BUILD THEM WITH GULP
│   ├── config
│   ├── layout
│   ├── locales
│   ├── sections
│   ├── snippets
│   └── templates
│       └── customers
└── src - these are the build files
    ├── fonts
    ├── iconfont
    ├── icons
    ├── img - These will run through an image optimisation process
    ├── pug
    │   └── snippets - I was previously building everything from pug before introducing liquid logic
    ├── sass
    │   ├── app - Custom styles on the site should be done within this folder
    │   │   ├── base
    │   │   ├── components
    │   │   ├── elements
    │   │   ├── layout
    │   │   ├── pages
    │   │   └── utils
    │   ├── shared - Variables/helpers that are shared between app and vendors
    │   └── vendors - Simply 'loads' other vendors, no custom styles/hacks please!
    │       ├── bulma
    │       │   ├── base
    │       │   ├── components
    │       │   ├── elements
    │       │   ├── grid
    │       │   ├── layout
    │       │   └── utilities
    │       ├── glide
    │       └── slick
    ├── scripts
    │   ├── app - All scripts written for the site. Add new files to config.json so they're loaded.
    │   └── vendors - Vendor scripts, no custom scripts/hacks here please!
    ├── static_assets - These are simply copied to the shopify/assets folders.
    └── svg
</pre>
