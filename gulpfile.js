// Require Plugins
var autoprefixer = require('gulp-autoprefixer'),
    cached = require('gulp-cached'),
    changed = require('gulp-changed'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    data = require('gulp-data'),
    del = require('del'),
    gulp = require('gulp'),
    gulpif = require('gulp-if'),
    gutil = require('gulp-util'),
    header = require('gulp-header'),
    iconfont = require('gulp-iconfont'),
    imagemin = require('gulp-imagemin'),
    pug = require('gulp-pug'),
    markdown = require('jstransformer-markdown-it'),
    jshint = require('gulp-jshint'),
    notify = require('gulp-notify'),
    package = require('./package'),
    path = require('path'),
    plumber = require('gulp-plumber'),
    prettify = require('gulp-prettify'),
    rename = require('gulp-rename'),
    render = require('gulp-nunjucks-render')
    sass = require('gulp-sass'),
    sort = require('gulp-sort'),
    svgmin = require('gulp-svgmin'),
    svgstore = require('gulp-svgstore'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch')
    url = require('url');

// Config
var config = require('./config');
var production = false;

// Error Handling
var errorHandler = function (err) {
  gutil.beep();
  notify("Something is Wrong!")
  console.log(err.toString());
  if (typeof this.emit === 'function') this.emit('end')
};

var liquidFilter = function(text, options){
  return "{{ " + text + " }}";
}

var noFilter = function(text, options){
  return "\n" + text;
}

gulp.task('custom-styles', function() {
  return gulp.src(path.join(config.css.src, '/**/*'), { nodir: true })
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(sass({ includePaths: [config.css.src] }))
    .pipe(autoprefixer({ "browsers": ["last 3 versions"] }))
    .pipe(gulpif(production, cssnano({ "autoprefixer": false })))
    .pipe(rename(
      {
        prefix: '_',
        extname: '.scss.liquid'
      }
    ))
    .pipe(gulpif(!production, cached('sass_compile')))
    .pipe(gulp.dest(config.paths.assets_dist))
});

gulp.task('pug', function() {
  return gulp.src(path.join(config.pug.src, '/**/*'), { nodir: true })
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(pug({
      pretty: false,
      filters: {
        liq: liquidFilter,
        nf: noFilter
      },
      filterAliases: {
        md: 'markdown-it'
      }
    }))
    .pipe(rename({
        extname: ".liquid"
    }))
    .pipe(
      prettify({
        indent_size: 2,
        indent_handlebards: true,
        // preserve_newlines: true,
        // max_preserve_new_lines: 250
        // indent_inner_html: true
      })
    )
    .pipe(header(
        "{% comment %} Please do not edit directly! Run gulp and edit files in the `${pug_dir}` source directory! {% endcomment %}\n",
        {
          pug_dir: config.pug.src}
      )
    )
    .pipe(gulp.dest(config.paths.base))
});

// Vendor styles can be used if we're pulling in assets from bower,
// else, you can just drop them off in the sass/vendor folder and load
// files them in the app.sass file.
gulp.task('vendor-styles', function() {
  return gulp.src(path.join(config.vendor_styles.src, '/**/*'), { nodir: true })
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(sass({ includePaths: [config.vendor_styles.includes] }))
    .on('error', sass.logError)
    .pipe(autoprefixer({ "browsers": ["last 3 versions"] }))
    .pipe(gulpif(production, cssnano({ "autoprefixer": false })))
    .pipe(gulp.dest(config.paths.assets_dist))
});

gulp.task('vendor-scripts', function() {
  return gulp.src(config.js.vendors, { cwd: config.js.vendor_src })
    .pipe(concat('_vendors.js'))
    .pipe(gulpif(production, uglify()))
    .pipe(gulp.dest(config.paths.assets_dist))
});

gulp.task('custom-js', function() {
  return gulp.src(config.js.appscripts, { cwd: config.js.src })
    .pipe(jshint())
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(concat('_app.js.liquid'))
    .pipe(gulpif(production, uglify()))
    .pipe(gulpif(!production, cached('js_compile')))
    .pipe(gulp.dest(config.paths.assets_dist))
});

gulp.task('images', function() {
  return gulp.src(config.images.src + '/**/*')
    .pipe(changed(config.paths.assets_dist)) // Ignore unchanged files
    .pipe(imagemin())
    .pipe(gulp.dest(config.paths.assets_dist))
});

gulp.task('fonts', function() {
  return gulp.src(config.fonts.src + '/**/*')
    .pipe(changed(config.paths.assets_dist)) // Ignore unchanged files
    .pipe(gulp.dest(config.paths.assets_dist))
});

// Blindly copy static assets from src folder to the assets folder
gulp.task('static_assets', function() {
  return gulp.src(config.static_assets.src + '/**/*')
    .pipe(changed(config.paths.assets_dist)) // Ignore unchanged files
    .pipe(gulp.dest(config.paths.assets_dist))
});

gulp.task('svg', function() {
  return gulp.src(path.join(config.images.svg, '/**/*.svg'))
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(changed(config.paths.assets_dist)) // Ignore unchanged files
    .pipe(svgmin(function(file) {
      var prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        multipass: true,
        precision: 2,
        js2svg: {
          pretty: true
        },
        plugins: [{
            removeTitle: true
          },
          {
            removeViewBox: false
          },
          {
            removeRasterImages: false
          },
          {
            cleanupListOfValues: false
          },
          {
            cleanupIDs: {
              prefix: prefix + '-',
              minify: true
            }
          }
        ]
      }
    }))
    .pipe(gulp.dest(config.paths.assets_dist))
});

// Clean
gulp.task('clean_assets', function() {
  return del(config.paths.assets_dist);
});

gulp.task('default', [], function() {
  gulp.start('iconfont', 'pug', 'custom-styles', 'vendor-styles', 'vendor-scripts', 'custom-js', 'images', 'svg', 'fonts', 'static_assets', 'watch');
});

gulp.task('production', ['clean_assets'], function() {
  production = true;
  gulp.start('iconfont', 'pug', 'custom-styles', 'vendor-styles', 'vendor-scripts', 'custom-js', 'images', 'svg', 'fonts', 'static_assets');
});

var settings = {
  name: package.name + ' icons',
  src: path.join( config.iconfont.src, '/*.svg'),
  dest: config.paths.assets_dist,
  sassDest: config.paths.assets_dist,
  template: config.iconfont.template,
  sassOutputName: '_i-icons.scss.liquid',
  fontPath: '.',
  options: {
    timestamp: 0, // see https://github.com/fontello/svg2ttf/issues/33
    fontName: 'i-icons',
    normalize: true,
    formats: config.iconfont.extensions
  }
}

gulp.task('iconfont', function(){
  return gulp.src(settings.src)
    .pipe(iconfont(settings.options))
    .on('glyphs', generateIconSass(settings))
    .on('error', errorHandler)
    .pipe(gulp.dest(settings.dest))
});

// Generate Icons
var generateIconSass = function(iconconfig) {
  return function(glyphs, options) {
    gutil.log(gutil.colors.blue('Generating ' + iconconfig.sassDest + '/' + iconconfig.sassOutputName))
    render.nunjucks.configure(iconconfig.nunjucks, { watch: false })

    return gulp.src(iconconfig.template)
      .pipe(data({
        icons: glyphs.map(function(glyph) {
          gutil.log(gutil.colors.green('+ adding ' + glyph.name + ' glyph'))
          return {
            name: glyph.name,
            code: glyph.unicode[0].charCodeAt(0).toString(16).toUpperCase()
          }
        }),

        fontName: iconconfig.options.fontName,
        fontPath: iconconfig.fontPath,
        comment: 'Generated from ' + iconconfig.template
      }))
      .pipe(render({
        path: iconconfig.template,
        envOptions: {
          tags: {
              blockStart: '<%',
              blockEnd: '%>',
              variableStart: '<<',
              variableEnd: '>>',
              commentStart: '<#',
              commentEnd: '#>'
          }
        }
      }))
    .on('error', errorHandler)
    .pipe(rename(iconconfig.sassOutputName))
    .pipe(gulp.dest(iconconfig.sassDest))
  }
}

gulp.task('watch', function() {
  watch(path.join(config.pug.src, '/**/*.pug'), function() {
    gulp.start('pug')
  });
  watch(path.join(config.iconfont.src, '/**/*.svg'), function() {
    gulp.start('iconfont')
  });
  watch(path.join(config.css.src, '/**/*.sass'), function() {
    gulp.start('custom-styles')
  });
  watch(path.join(config.static_assets.src, '/**/*'), function() {
    gulp.start('static_assets');
  });
  watch(path.join(config.js.src, '/**/*'), function() {
    gulp.start('custom-js')
  });
  watch(path.join(config.images.src, '/**/*'), function() {
    gulp.start('images')
  });
  watch(path.join(config.images.svg, '/**/*.svg'), function() {
    gulp.start('svg')
  });
});
