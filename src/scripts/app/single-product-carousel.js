$(document).ready(function(){
   $('.product-images-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<i class="i-icon left is-24 slick-left"></i>',
    nextArrow: '<i class="i-icon right is-24 slick-right"></i>',
    fade: true,
    asNavFor: '.product-images-carousel-thumbs'
  });
  $('.product-images-carousel-thumbs').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-images-carousel',
    dots: true,
    centerMode: true,
    focusOnSelect: true
  });
})
