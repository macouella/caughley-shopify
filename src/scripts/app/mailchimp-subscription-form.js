jQuery(document).ready(function($) {
  'use strict';

  // Hide all messages
  $('.newsletter .success').hide();
  $('.newsletter .error').hide();

  // Mailchimp form, a class-based one so we can
  // have more than one on the page
  $('.newsletter form').submit(function(e) {
    e.preventDefault();

    var $form      = $(this);
    var $formWrap = $form.closest('.newsletter');

    var $error     = $formWrap.find('.error');
    var $modal     = $formWrap.closest('.modal');
    var $success   = $formWrap.find('.success');
    var action     = $form.attr('action');
    var formData   = $form.serialize();
    var isModal    = $modal.length > 0;

    // Hide messages before submitting
    $success.hide();
    $error.hide();

    $.ajax({
      type: "GET",
      url: action,
      data: formData,
      cache: false,
      dataType: "jsonp",
      jsonp: "c", // trigger MailChimp to return a JSONP response
      contentType: "application/json; charset=utf-8",
      error: function(err) {
        $error.text("Can't connect to server.").fadeIn(300);
      },
      success: function(data) {
        console.log(data);
        if (data.result != "success") {
          $error.fadeIn(300);
          // For devs, uncomment to troubleshoot
          // console.log(data);
        } else {
          // It worked, carry on...
          $form.find('input').val('');
          $success.fadeIn(300);
          // Triggered via modal... so we'll close it as well
          if(isModal){
            // Let the user know that we'll auto-close the modal
            setTimeout(function(){
              // Handle modals if they exist
              $modal.removeClass('is-active');
              $('html').removeClass('is-clipped');
            }, 2000);
          }
        }
      }
    });
  }); // end submit
});
