$(document).ready(function(){
  var $onboardingModal = $('.modal.for-onboarding');
  var pagesVisited = Cookies.get('pages_visited') !== undefined ?
    parseInt(Cookies.get('pages_visited')) : 0;
  var onboardingModalViewed = Cookies.get('onboarding_list') !== undefined;
  var isPreview = $('.onboarding-modal-previewed').length;

  var incrementPagesVisit = function(){
    // Check the navigation time API if we're just refreshing the page
    if (performance.navigation.type == 1) {
      // console.info( "This page is reloaded" );
    } else {
      // If it's not a page reload, we can increment the pagesVisited
      Cookies.set('pages_visited', pagesVisited + 1);
      console.info( "This page is not reloaded");
    }
  }

  var toggleModal = function(remove = false) {
    if(remove){
      $('html').removeClass('is-clipped');
      $onboardingModal.removeClass('is-active');
    }else {
      $('html').toggleClass('is-clipped');
      $onboardingModal.toggleClass('is-active');
    }
  }

  $('.modal-background, .close-modal').click(toggleModal);

  $(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
      toggleModal(true);
    }
  });

  // Toggle modal if it hasn't been
  // disabled yet and the third page is being viewed.
  if((!onboardingModalViewed && pagesVisited > 1) || isPreview){
    toggleModal();
    Cookies.set('onboarding_list', '1', { expires: 30 });
  }else{
    // Let's increment it for next time!
    incrementPagesVisit();
  }
});
