$(document).ready(function($) {
  'use strict';
  $('.glide').each(function(){
    var $glider = $(this);
    var updateGlideBulletsTheme = function(event){
      console.log(event);
      var $currentSlide = event.current;
      // Toggle nav colour states. Let's clean it up!
      var $gliderBullets = $('.glide__bullets');
      if($currentSlide.hasClass('is-dark')){
        $gliderBullets.removeClass('is-light');
        $gliderBullets.addClass('is-dark');
      }else{
        $gliderBullets.removeClass('is-dark');
        $gliderBullets.addClass('is-light');
      }
    }
    $glider.glide({
      type: 'slideshow',
      autoplay: 7000,
      animationDuration: 500,
      hoverpause: true,
      animationTimingFunc: "cubic-bezier(0.645, 0.045, 0.355, 1)",
      afterTransition: updateGlideBulletsTheme
    });
  });
});
