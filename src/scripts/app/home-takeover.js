$(document).ready(function(){
  var $takeover = $('.hero.for-home-takeover');
  var isPreview = $('.takeover-previewed').length;
  var cookieName = 'home_takeover';
  var takeoverViewed = Cookies.get(cookieName) !== undefined;

  if(!takeoverViewed || isPreview){
    setTimeout(function(){
      $('html').addClass('is-clipped');
      $takeover.addClass('is-active');
      Cookies.set(cookieName, '1', { expires: 30 });
    }, 1000);
    $takeover.on('click touchstart', '.toggle', function(){
      $takeover.addClass('is-offscreen');
      $('html').removeClass('is-clipped');
    });
  }else if($takeover.hasClass('is-admin')) {
    // Enable admin preview
    $('html').addClass('is-clipped');
    $takeover.addClass('is-active');
  }else{
    // Cleanup :)
    $takeover.remove();
  }
});
